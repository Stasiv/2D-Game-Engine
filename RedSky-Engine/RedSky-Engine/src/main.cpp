#include <iostream>

#include "core/event/event.h"
#include "core/event/event_manager.h"

#include "core/entry point/entry_point.h"

#include "core/layer/layer_stack.h"

#include "core/math/vector/vector2D.h"

#include "core/debug/console/rs_console.h"

void EventBeginPlay()
{
	RS_KERNEL::RS_DEBUG::Console console;
	console.Attach();
	console.TextColor(RS_KERNEL::RS_DEBUG::RS_CC::RED);
}

void EventEachTick()
{
	std::cout << "ET\n";
}

void Main()
{
	RS::EventManager::OnEvent("BeginPlay", EventBeginPlay);
	//RS::EventManager::OnEvent("EachTick", EventEachTick);
}
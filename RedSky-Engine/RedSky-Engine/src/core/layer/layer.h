#pragma once

#include <string>

namespace RS_KERNEL
{
	class Layer
	{
	protected:
		std::string Name;
	public:
		Layer(const std::string name)
		{
			Name = name;
		}

		virtual ~Layer()
		{

		}

		virtual void OnAttach()
		{

		}

		virtual void OnDetach()
		{

		}

		virtual void OnUpdate()
		{

		}
		 
		std::string GetName() const { return Name; }
	};
}
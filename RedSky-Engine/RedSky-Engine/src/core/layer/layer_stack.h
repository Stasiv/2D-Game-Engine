#pragma once

#include <map>
#include <iterator>

#include "layer.h"

namespace RS_KERNEL
{
	class LayerStack
	{
	private:
		std::multimap<std::string, Layer*> Layers;
	public:
		LayerStack()
		{

		}

		void Attach(const Layer* layer)
		{
			//TODO add name check

			Layers.insert(std::make_pair(layer->GetName(), const_cast<Layer*>(layer)));
			const_cast<Layer*>(layer)->OnAttach();
		}

		void Detach(const std::string name)
		{
			//TODO add check

			auto find = Layers.find(name);

			if (find != Layers.end());
			{
				find->second->OnDetach();
				Layers.erase(find->first);
			}
		}

		void Update()
		{
			for (std::multimap<std::string, Layer*>::iterator i = Layers.begin(); i != Layers.end(); ++i)
				i->second->OnUpdate();
		}
	
	};
}
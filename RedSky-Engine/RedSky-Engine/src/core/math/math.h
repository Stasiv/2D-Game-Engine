#pragma once

#include <math.h>

namespace RS_MATH
{
	#define RS_PI	3.14159265359

	inline float sqrt(const float value)
	{
		return ::sqrt(value);
	}

	inline float pow(const float value, const float pow)
	{
		return ::pow(value, pow);
	}
}
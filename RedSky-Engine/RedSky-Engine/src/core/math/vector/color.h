#pragma once

#include "../math.h"

namespace RS_MATH
{
	struct  Color
	{
		float x, y, z, w;

		float& r = x;
		float& g = y;
		float& b = z;
		float& a = w;

		Color()
		{
			x = y = z = w = 0.0f;
		}

		Color(const float value)
		{
			x = y = z = w = value;
		}

		Color(const float x, const float y, const float z, const float w)
		{
			this->x = x;
			this->y = y;
			this->z = z;
			this->w = w;
		}

		Color(const Color& vector)
		{
			x = vector.x;
			y = vector.y;
			z = vector.z;
			w = vector.w;
		}

		void operator = (const Color vector)
		{
			x = vector.x;
			y = vector.y;
			z = vector.z;
			w = vector.w;
		}

		Color operator + (const Color vector) const
		{
			Color result;

			result.x = x + vector.x;
			result.y = y + vector.y;
			result.z = z + vector.z;
			result.w = w + vector.w;

			return result;
		}

		Color operator - (const Color vector) const
		{
			Color result;

			result.x = x - vector.x;
			result.y = y - vector.y;
			result.z = z - vector.z;
			result.w = w - vector.w;

			return result;
		}


		Color operator * (const Color vector) const
		{
			Color result;

			result.x = x * vector.x;
			result.y = y * vector.y;
			result.z = z * vector.z;
			result.w = w * vector.w;

			return result;
		}


		Color operator / (const Color vector) const
		{
			Color result;

			result.x = x / vector.x;
			result.y = y / vector.y;
			result.z = z / vector.z;
			result.w = w / vector.w;

			return result;
		}

		void operator += (const Color vector)
		{
			*this = *this + vector;
		}

		void operator -= (const Color vector)
		{
			*this = *this - vector;
		}

		void operator *= (const Color vector)
		{
			*this = *this * vector;
		}

		void operator /= (const Color vector)
		{
			*this = *this / vector;
		}
	};

	inline float length(const Color vector)
	{
		return sqrt(pow(vector.x, 2) +
			pow(vector.y, 2) +
			pow(vector.z, 2) +
			pow(vector.w, 2));
	}

	inline Color normalize(const Color vector)
	{
		Color result;

		result.x = vector.x / length(vector);
		result.y = vector.y / length(vector);
		result.z = vector.z / length(vector);
		result.w = vector.w / length(vector);

		return result;
	}
}
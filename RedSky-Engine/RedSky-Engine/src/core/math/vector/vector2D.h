#pragma once

#include "../math.h"

namespace RS_MATH
{
	struct Vector2D
	{
		float x, y;

		Vector2D()
		{
			x = y = 0.0f;
		}

		Vector2D(const float value)
		{
			x = y = value;
		}

		Vector2D(const float x, const float y)
		{
			this->x = x;
			this->y = y;
		}

		Vector2D(const Vector2D& vector)
		{
			x = vector.x;
			y = vector.y;
		}

		void operator = (const Vector2D vector)
		{
			x = vector.x;
			y = vector.y;
		}

		Vector2D operator + (const Vector2D vector) const
		{
			Vector2D result;

			result.x = x + vector.x;
			result.y = y + vector.y;

			return result;
		}

		Vector2D operator - (const Vector2D vector) const
		{
			Vector2D result;

			result.x = x - vector.x;
			result.y = y - vector.y;

			return result;
		}


		Vector2D operator * (const Vector2D vector) const
		{
			Vector2D result;

			result.x = x * vector.x;
			result.y = y * vector.y;

			return result;
		}


		Vector2D operator / (const Vector2D vector) const
		{
			Vector2D result;

			result.x = x / vector.x;
			result.y = y / vector.y;

			return result;
		}

		void operator += (const Vector2D vector)
		{
			*this = *this + vector;
		}

		void operator -= (const Vector2D vector)
		{
			*this = *this - vector;
		}

		void operator *= (const Vector2D vector)
		{
			*this = *this * vector;
		}

		void operator /= (const Vector2D vector)
		{
			*this = *this / vector;
		}
	};

	inline float length(const Vector2D vector)
	{
		return sqrt(pow(vector.x, 2) + pow(vector.y, 2));
	}

	inline Vector2D normalize(const Vector2D vector)
	{
		Vector2D result;

		result.x = vector.x / length(vector);
		result.y = vector.y / length(vector);

		return result;
	}

	inline float dot(const Vector2D vector1, const Vector2D vector2)
	{
		return ((vector1.x * vector2.x) + (vector1.y * vector2.y));
	}
}
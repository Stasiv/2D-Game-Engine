#pragma once

#include "../math.h"

namespace RS_MATH
{
	struct Vector3D
	{
		float x, y, z;

		float& r = x;
		float& g = y;
		float& b = z;

		Vector3D()
		{
			x = y = z = 0.0f;
		}

		Vector3D(const float value)
		{
			x = y = z = value;
		}

		Vector3D(const float x, const float y, const float z)
		{
			this->x = x;
			this->y = y;
			this->z = z;
		}

		Vector3D(const Vector3D& vector)
		{
			x = vector.x;
			y = vector.y;
			z = vector.z;
		}

		void operator = (const Vector3D vector)
		{
			x = vector.x;
			y = vector.y;
			z = vector.z;
		}

		Vector3D operator + (const Vector3D vector) const
		{
			Vector3D result;

			result.x = x + vector.x;
			result.y = y + vector.y;
			result.z = z + vector.z;

			return result;
		}

		Vector3D operator - (const Vector3D vector) const
		{
			Vector3D result;

			result.x = x - vector.x;
			result.y = y - vector.y;
			result.z = z - vector.z;

			return result;
		}


		Vector3D operator * (const Vector3D vector) const
		{
			Vector3D result;

			result.x = x * vector.x;
			result.y = y * vector.y;
			result.z = z * vector.z;

			return result;
		}


		Vector3D operator / (const Vector3D vector) const
		{
			Vector3D result;

			result.x = x / vector.x;
			result.y = y / vector.y;
			result.z = z / vector.z;

			return result;
		}

		void operator += (const Vector3D vector)
		{
			*this = *this + vector;
		}

		void operator -= (const Vector3D vector)
		{
			*this = *this - vector;
		}

		void operator *= (const Vector3D vector)
		{
			*this = *this * vector;
		}

		void operator /= (const Vector3D vector)
		{
			*this = *this / vector;
		}
	};

	inline float length(const Vector3D vector)
	{
		return sqrt(pow(vector.x, 2) + 
					pow(vector.y, 2) + 
					pow(vector.z, 2));
	}

	inline Vector3D normalize(const Vector3D vector)
	{
		Vector3D result;

		result.x = vector.x / length(vector);
		result.y = vector.y / length(vector);
		result.z = vector.z / length(vector);

		return result;
	}

	inline float dot(const Vector3D vector1, const Vector3D vector2)
	{
		return ((vector1.x * vector2.x) + 
				(vector1.y * vector2.y) + 
				(vector1.z * vector2.z));
	}
}
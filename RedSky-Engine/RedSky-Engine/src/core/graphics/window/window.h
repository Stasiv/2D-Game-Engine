#pragma once

#include "base_window.h"
#include "../../math/vector/vector2D.h"

namespace RS_KERNEL
{
	namespace RS_GRAPHICS
	{
		class Window
		{
		public:
			std::string Title;
			RS_MATH::Vector2D Size;

			bool Fullscreen;

			static BaseWindow* BW;
		public:
			Window()
			{
				Title = "RedSky-Engine";
				Size = RS_MATH::Vector2D(1280, 720);
				Fullscreen = false;
			}

			Window(const char* title, const RS_MATH::Vector2D size)
			{
				Title = title;
				Size = size;
				Fullscreen = false;
			}

			~Window()
			{
				delete BW;
			}

			static void Initialize(HINSTANCE hinstance, int cmdShow)
			{
				BW->hInstance = hinstance;
				BW->nCmdShow = cmdShow;
			}

			void Create()
			{
				//TODO add check
				BW->Create(Title.c_str(), Size.x, Size.y);
			}

			void Update()
			{
				BW->HandleMessage();
			}

			void SetFullscreen(const bool mode)
			{
				if (mode)
				{
					SetWindowLong(BW->hwnd, GWL_STYLE, WS_POPUP);
					SetWindowLong(BW->hwnd, GWL_EXSTYLE, WS_EX_TOPMOST);
					ShowWindow(BW->hwnd, SW_SHOWMAXIMIZED);

					Fullscreen = true;
				}
				else
				{
					SetWindowLong(BW->hwnd, GWL_STYLE, WS_OVERLAPPEDWINDOW | WS_VISIBLE);
					SetWindowLong(BW->hwnd, GWL_EXSTYLE, 0L);
					ShowWindow(BW->hwnd, SW_SHOWDEFAULT);

					Fullscreen = false;
				}
			}

			void SetCursorPosition(const RS_MATH::Vector2D position)
			{
				POINT point;
				point.x = position.x;
				point.y = position.y;

				ClientToScreen(BW->hwnd, &point);
				SetCursorPos(point.x, point.y);
			}

			void SetTitle(const char* title)
			{
				Title = title;
				SetWindowText(BW->hwnd, title);
			}

			void SetSize(const RS_MATH::Vector2D size)
			{
				SetWindowPos(BW->hwnd, HWND_TOPMOST, 0, 0, size.x, size.y, NULL);
			}

			bool ShouldClose() const
			{
				return IsWindow(BW->hwnd);
			}

			RS_MATH::Vector2D GetCursorPosition() const
			{
				POINT point;

				if (GetCursorPos(&point))
				{
					ScreenToClient(BW->hwnd, &point);
					return RS_MATH::Vector2D(point.x, point.y);
				}

				return RS_MATH::Vector2D(0.0f, 0.0f);
			}

			inline RS_MATH::Vector2D GetSize() const { return Size; }
			inline std::string GetTitle() const { return Title; }

		};

		BaseWindow* Window::BW = new BaseWindow;
	}
}
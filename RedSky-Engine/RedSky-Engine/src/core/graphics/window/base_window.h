#pragma once

#include <windows.h>

namespace RS_KERNEL
{
	namespace RS_GRAPHICS
	{
		LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

		struct BaseWindow
		{
			HINSTANCE hInstance;
			int nCmdShow;

			MSG Msg;
			WNDCLASSEX wc;
			HWND hwnd;

			int Create(const char* title, const int width, const int height)
			{
				//TODO add crash message

				wc.cbSize = sizeof(WNDCLASSEX);
				wc.style = 0;
				wc.lpfnWndProc = WndProc;
				wc.cbClsExtra = 0;
				wc.cbWndExtra = 0;
				wc.hInstance = hInstance;
				wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
				wc.hCursor = LoadCursor(NULL, IDC_ARROW);
				wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
				wc.lpszMenuName = NULL;
				wc.lpszClassName = "RS_Engine";
				wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

				if (!RegisterClassEx(&wc))
				{
					MessageBox(NULL, "Window Registration Failed!", "Error!",
						MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}

				hwnd = CreateWindowEx(
					WS_EX_CLIENTEDGE,
					wc.lpszClassName,
					title,
					WS_OVERLAPPEDWINDOW,
					0, 0, width, height,
					NULL, NULL, hInstance, NULL);

				if (hwnd == NULL)
				{
					MessageBox(NULL, "Window Creation Failed!", "Error!",
						MB_ICONEXCLAMATION | MB_OK);
					return -1;
				}

				ShowWindow(hwnd, nCmdShow);
				UpdateWindow(hwnd);

				return 0;
			}

			void HandleMessage()
			{
				while (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE))
				{
					TranslateMessage(&Msg);
					DispatchMessage(&Msg);
				}
			}
		};

		LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
		{
			switch (msg)
			{
			case WM_CLOSE:
				DestroyWindow(hwnd);
				break;
			case WM_DESTROY:
				PostQuitMessage(0);
				break;
			default:
				return DefWindowProc(hwnd, msg, wParam, lParam);
			}
			return 0;
		}
	}
}
#pragma once

#include "../../event/application_event.h"
#include "../../layer/layer_stack.h"

namespace RS_KERNEL
{
	class Application
	{
	private:
		LayerStack ApplicationEventLayerStack;
	public:
		Application()
		{

		}

		~Application()
		{

		}

		void BeginPlay()
		{
			ApplicationEventLayerStack.Attach(BPL);
			ApplicationEventLayerStack.Detach(BPL->GetName());

			delete BPL;
		}

		void EeachTick()
		{
			ApplicationEventLayerStack.Attach(ETL);
			ApplicationEventLayerStack.Detach(ETL->GetName());
		}
	};
}
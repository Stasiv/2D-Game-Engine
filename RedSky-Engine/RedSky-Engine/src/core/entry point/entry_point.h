#pragma once

#include <windows.h>

#include "application/application.h"

#include "../graphics/window/window.h"

extern void Main();

int WINAPI 
	WinMain(HINSTANCE hInstance, 
			HINSTANCE hPrevInstance,
			LPSTR lpCmdLine, 
			int nCmdShow)
{
	RS_KERNEL::RS_GRAPHICS::Window::Initialize(hInstance, nCmdShow);

	RS_KERNEL::RS_GRAPHICS::Window window("Game", RS_MATH::Vector2D(1280, 720));
	window.Create();

	RS_KERNEL::Application app;

	Main();	//User code

	app.BeginPlay();

	RS_MATH::Vector2D cc;

	while (window.ShouldClose())
	{
		window.Update();

		app.EeachTick();
	}

	return 0;
}
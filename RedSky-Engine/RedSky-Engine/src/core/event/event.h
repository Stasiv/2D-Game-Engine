#pragma once

#include <vector>
#include <functional>
#include <string>

namespace RS
{
	class Event
	{
	private:
		std::string Name;

		std::vector <std::function<void()>> Functions;
	public:
		Event(const std::string name)
		{
			Name = name;
		}

		void Call()
		{
			for (auto element : Functions)
				element();
		}

		void OnEvent(void(*function)())
		{
			//TODO check

			Functions.push_back(function);
		}

		std::string GetName() const { return Name; }
	};
}
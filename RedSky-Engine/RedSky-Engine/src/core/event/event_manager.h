#pragma once

#include <map>

#include "event.h"

namespace RS
{
	class EventManager
	{
	private:
		static std::multimap<std::string, Event*> Events;
	public:
		static void AddEvent(const Event* event)
		{
			//TODO check

			Events.insert(std::make_pair(event->GetName(), const_cast<Event*>(event)));
		}

		static void OnEvent(const std::string name, void(*function)())
		{
			auto find = Events.find(name);

			if (find != Events.end())
				find->second->OnEvent(function);

			//TDOO error message
		}
	};

	std::multimap<std::string, Event*> EventManager::Events;
}
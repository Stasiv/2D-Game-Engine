#pragma once

#include "../layer/layer.h"
#include "event_manager.h"

namespace RS_KERNEL
{
	class BeginPlayLayer : public Layer
	{
	private:
		RS::Event* Event;
	public:
		BeginPlayLayer() : Layer("BeginPlayLayer")
		{
			Event = new RS::Event("BeginPlay");
			RS::EventManager::AddEvent(Event);
		}

		~BeginPlayLayer() override
		{
			delete Event;
		}

		void OnAttach() override
		{
			Event->Call();
		}

		void OnDetach() override
		{

		}

		void OnUpdate() override
		{

		}
	};

	BeginPlayLayer* BPL = new BeginPlayLayer;

	class EeachTickLayer : public Layer
	{
	private:
		RS::Event* Event;
	public:
		EeachTickLayer() : Layer("EachTickLayer")
		{
			Event = new RS::Event("EachTick");
			RS::EventManager::AddEvent(Event);
		}

		~EeachTickLayer() override
		{
			delete Event;
		}

		void OnAttach() override
		{
			Event->Call();
		}

		void OnDetach() override
		{

		}

		void OnUpdate() override
		{

		}
	};

	EeachTickLayer* ETL = new EeachTickLayer;
}
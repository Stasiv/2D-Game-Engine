#include <windows.h>

#include "crash_message.h"

namespace RS_KERNEL
{
	namespace RS_DEBUG
	{
		void CrashMessage(const wchar_t* title, const wchar_t* message)
		{
			MessageBeep(MB_ICONSTOP);
			MessageBoxW(NULL, message, title, MB_OK | MB_ICONHAND);
		}
	}
} 
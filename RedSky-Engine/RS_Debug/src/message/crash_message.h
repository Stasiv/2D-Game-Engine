#pragma once

#include"../rs_debug.h"

#include <iostream>

namespace RS_KERNEL
{
	namespace RS_DEBUG
	{
		RS_API void CrashMessage(const wchar_t* title, const wchar_t* message);
	}
}
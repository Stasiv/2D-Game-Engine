#pragma once

#include <windows.h>
#include <cstdio>

#include "../rs_debug.h"

namespace RS_KERNEL
{
	namespace RS_DEBUG
	{
		class RS_API Console
		{
		private:
			CONSOLE_FONT_INFOEX CFI;
		public:
			Console();

			bool Attach();
			void Detach();

			void TextColor(int color);
		};
	}
}
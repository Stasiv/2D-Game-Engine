#include "console.h"

namespace RS_KERNEL
{
	namespace RS_DEBUG
	{
		Console::Console()
		{

		}

		bool Console::Attach()
		{
			AllocConsole();
			AttachConsole(GetCurrentProcessId());
			if (!freopen("CON", "w", stdout))
				return false;
			return true;
		}

		void Console::Detach()
		{
			HWND console = GetConsoleWindow();
			ShowWindow(console, 0);
			FreeConsole();
		}

		void Console::TextColor(int color)
		{
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color);
		}
	}
}
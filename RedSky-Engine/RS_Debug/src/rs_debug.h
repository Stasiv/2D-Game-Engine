#pragma once

#ifdef DEBUG_DLL
	#define RS_API __declspec(dllexport)
#else
	#define RS_API __declspec(dllimport)
#endif
